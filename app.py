from flask import Flask
from flask_restful import Api, Resource, reqparse
from requests import get
from bs4 import BeautifulSoup
from json import JSONEncoder
import json
url = 'http://www.imdb.com/search/title?release_date=2017&sort=num_votes,desc&page=1'

response = get(url)
print(response.text[:500])

html_soup = BeautifulSoup(response.text, 'html.parser')
type(html_soup)

movie_containers = html_soup.find_all('div', class_ = 'lister-item mode-advanced')
print(type(movie_containers))
print(len(movie_containers))

# Lists to store the scraped data in
names = []
years = []
ratings = []
metascores = []
votes = []
genres = []

movieList = []

movieSpecialList = []

class Movie(Resource):
        def __init__(self, name, year, ratings, metascore, votes):
		self.name = name
		self.year = year
		self.ratings = ratings
		self.metascore = metascore
		self.votes = votes
        def toJson(self):
                data = {}
                data['name'] = self.name 
                return data 
                #return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
        def __str__(self):
                return "Name: {}, Year: {}, Ratings: {}, Metascore: {}, Votes: {}".format(name, year, ratings, metascores, votes)
class MovieList(Resource):
	def get(self):
                jsonList = []
                for item in movieList:
                        jsonList.append(item.toJson())
                print(jsonList)
                return json.dumps(jsonList), 200
class MovieSpecialList(Resource):
        def get(self):
                jsonList = []
                for item in movieSpecialList:
                        jsonList.append(item.toJson())
                print(jsonList)
                return json.dumps(jsonList), 200
#class MyEncoder(JSONEncoder):
 #       def default(self, o):
  #              return o.__dict__    
# Extract data from individual movie container
for container in movie_containers:

    # If the movie has Metascore, then extract:
    if container.find('div', class_ = 'ratings-metascore') is not None:

        # The namez
        name = container.h3.a.text
        names.append(name)

        # The year
        year = container.h3.find('span', class_ = 'lister-item-year').text
        years.append(year)
        print("year: " + year)

        # The IMDB rating
        imdb = float(container.strong.text)
        ratings.append(imdb)

        # The Metascore
        m_score = container.find('span', class_ = 'metascore').text
        metascores.append(int(m_score))

        # The number of votes
        vote = container.find('span', attrs = {'name':'nv'})['data-value']
        votes.append(int(vote))

        genre = container.find('span', class_ = 'genre').text
        print("genre: " + genre)
        movie = Movie(name, year, imdb, m_score, vote)
	movieList.append(movie)

        if (str(year) == "(2017)" and "Comedy" in genre):
                movieSpecialList.append(movie)
                print(movieSpecialList)
import pandas as pd

test_df = pd.DataFrame({'movie': names,
                       'year': years,
                       'imdb': ratings,
                       'metascore': metascores,
                       'votes': votes})
print(test_df.info())
test_df


app = Flask(__name__)
api = Api(app)

api.add_resource(MovieList, "/movie")
api.add_resource(MovieSpecialList, "/movie/search")
#print(json.dumps(movieList[0]))
app.run(debug=True)